Name:           orpie
Version:        1.5.2
Release:        4%{?dist}
Summary:        A full-screen console-based RPN calculator application
ExcludeArch:    armv7hl

License:        GPLv2 GPLv2+ LGPLv2
URL:            http://freecode.com/projects/orpie
Source0:        https://github.com/pelzlpj/orpie/releases/download/release-1.5.2/%{name}-%{version}.tar.gz
# GSL version 2 introduced minor incompatibilities with GSL version 1.6. The patch addresses those.
# This is tracked  by upstream here:
# https://github.com/pelzlpj/orpie/issues/1
Patch0:         patch-gsl-mgsl_sf.c.diff
# For some reason, compiling against Ocaml 4.04 cause the Ocaml compiler to complain
# about 'caml__frame' undeclared. Some searching yielded a simple solution. Upstream was made aware here:
# https://github.com/pelzlpj/orpie/issues/10
Patch1:         patch-mlgsl_error.c.diff
# This file declared two functions path0 removed. Added here:
# https://github.com/pelzlpj/orpie/issues/1
Patch2:         patch-gsl_sf.ml.diff
# Patch 3 is a hacky workaround for upstream not using ocamlfind to link against gsl dependency dynamically.
# See pull request here: https://github.com/pelzlpj/orpie/pull/2
Patch3:         patch-Makefile.in.diff
# Because F26 adds -Werror=implicit-function-declaration
# Reported upstream here: https://github.com/pelzlpj/orpie/issues/11
Patch4:         patch-configure.diff
# Because F26 adds Werror=implicit-int
# Reported upstream here: https://github.com/pelzlpj/orpie/issues/11
Patch5:         patch-mlgsl_blas.h.diff

BuildRequires:  ocaml
BuildRequires:  gsl-devel
BuildRequires:  ncurses-devel
BuildRequires:  ocaml-camlp4-devel

%description
Orpie is a full-screen console-based RPN calculator that uses the curses
library.  Its operation is similar to that of modern HP calculators,
but data entry has been optimized for efficiency on a PC keyboard. Its
features include extensive scientific calculator functionality, command
completion, and a visible interactive stack.


%prep
%setup -q

%patch0 -p0
%patch1 -p0
%patch2 -p0
%patch3 -p0
%patch4 -p0
%patch5 -p0

%build
%configure
make


%install
make install DESTDIR=$RPM_BUILD_ROOT

%check || :
#make test
#make check


%files
%config(noreplace) %{_sysconfdir}/orpierc
%doc doc/manual.html doc/manual.pdf doc/manual.tex.in doc/TODO README ChangeLog
%license COPYING
%{_bindir}/*
%{_mandir}/man[^3]/*


%changelog
* Mon Dec 05 2016 Jared Wallace <jared-wallace@us.ibm.com> 1.5.2-4
- added two more patches because F26 made some changes to default compiler flags
* Tue Nov 29 2016 Jared Wallace <jared@jared-wallace.com> 1.5.2-3
- added three more patches to address issues exposed by Ocaml 4.04
* Fri Nov 11 2016 Jared Wallace <jared@jared-wallace.com> 1.5.2-2
- made release tag dynamic
- added additional licenses
- removed defattr line (unnecessary)
- removed hardened build flag (unnecessary)
- added multi-core compile flag
- changed COPYING from %doc to %license
* Wed Oct 05 2016 Jared Wallace <jared@jared-wallace.com> 1.5.2-1
- Cleaned up spec file
- Added patch to workaround GSL version 2 issue upstream
